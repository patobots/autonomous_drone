#ifndef ICP_H
#define ICP_H

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Imu.h>
#include <laser_geometry/laser_geometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <pcl/common/common.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/registration/icp.h>
#include <pcl_conversions/pcl_conversions.h>


class ICP
{
  public:
    ICP(ros::NodeHandle node, ros::NodeHandle private_nh);
    ~ICP() {};

  private:
    void lidarCallback(const sensor_msgs::LaserScan::ConstPtr& msg);
    void imuCallback(const sensor_msgs::Imu::ConstPtr& msg);
    void pclRegistration(const pcl::PointCloud<pcl::PointXYZ>::Ptr curr_cloud,
                        const pcl::PointCloud<pcl::PointXYZ>::Ptr prev_cloud);
     // Store Previous Point Cloud
    pcl::PointCloud<pcl::PointXYZ> _prev_cloud;
    Eigen::Matrix4f _prev_tf;
    //ROS topics
    ros::Subscriber lidar_sub;
    ros::Subscriber imu_sub;
    ros::Publisher  cp_pub;
    ros::Publisher  pose_pub;

    // first interation
    bool is_initial;
    bool is_imu_start;
    double _prev_acc, _curr_acc; //accleration in consecutive time stamps
    double _prev_imu_time, _curr_imu_time; //consecutive time stamps of imu data 
    double _diff_time;
    double _prev_time_stamp; //time stamp of the previous point cloud
    double _speed; //speed for initial guess
    double _yaw_rate; //change in yaw for initial guess
};


#endif
