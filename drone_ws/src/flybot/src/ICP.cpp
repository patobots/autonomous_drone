
#include "ICP.h"
#include <iostream>

using namespace std;

laser_geometry::LaserProjection projector_;

ICP::ICP(ros::NodeHandle node, ros::NodeHandle private_nh)
{

    lidar_sub = node.subscribe("/scan", 1, &ICP::lidarCallback, this);
    imu_sub = node.subscribe("/imu", 1, &ICP::imuCallback, this);
    cp_pub =    node.advertise<sensor_msgs::PointCloud2>("PointCloud", 1);
    pose_pub =  node.advertise<geometry_msgs::PoseWithCovarianceStamped>("icp_pose", 1);

    is_initial = true;
    is_imu_start = true;
}

void ICP::imuCallback(const sensor_msgs::Imu::ConstPtr& msg)
{
    if(is_imu_start)
    {
        _prev_acc = msg->linear_acceleration.x;
        _prev_imu_time = msg->header.stamp.toSec();

        is_imu_start = false;
    }
    else
    {
        _curr_acc = msg->linear_acceleration.x;
        _curr_imu_time = msg->header.stamp.toSec();

        double del_time = _curr_imu_time - _prev_imu_time;
        double avg_acc = 0.5*(_prev_acc + _curr_acc);

        _speed = avg_acc*del_time;
        _yaw_rate = msg->angular_velocity.z;

        _prev_acc = _curr_acc;
        _prev_imu_time = _curr_imu_time;
    }

    return;
}

void ICP::lidarCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
    sensor_msgs::PointCloud2 cloud;
    pcl::PointCloud<pcl::PointXYZ>::Ptr curr_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr prev_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>(_prev_cloud));
    
    projector_.projectLaser(*msg, cloud);

    pcl::fromROSMsg(cloud, *curr_cloud_ptr);

    _diff_time = msg->header.stamp.toSec() - _prev_time_stamp;

    // initial interaction
    if(is_initial)
    {
        is_initial = false;
        _prev_cloud = *curr_cloud_ptr;
        _prev_time_stamp = _diff_time;
    }
    else
        {
            pcl::fromROSMsg(cloud, *curr_cloud_ptr);
            pclRegistration(curr_cloud_ptr, prev_cloud_ptr);
            // _prev_cloud = *curr_cloud_ptr;
            _prev_tf = Eigen::Matrix4f::Identity();
            _prev_time_stamp = msg->header.stamp.toSec();
        }
    
    // Publish CloudPoint
    cloud.header.frame_id = "base_link";
    cloud.header.stamp = msg->header.stamp;
    cp_pub.publish(cloud);
    return;
}


void ICP::pclRegistration(const pcl::PointCloud<pcl::PointXYZ>::Ptr curr_cloud, 
                          const pcl::PointCloud<pcl::PointXYZ>::Ptr prev_cloud)
{
    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>);


    icp.setInputSource (curr_cloud);
    icp.setInputTarget (prev_cloud);
    icp.setTransformationEpsilon(0.008);
    icp.setMaxCorrespondenceDistance(1.5);
    icp.setEuclideanFitnessEpsilon(0.1);
    icp.setMaximumIterations (100);

    double diff_yaw = _diff_time*_yaw_rate;
    Eigen::AngleAxisf init_rotation (diff_yaw, Eigen::Vector3f::UnitZ ());
    double del_x = _diff_time*_speed;
    Eigen::Translation3f init_translation (del_x, 0.0, 0.0);
    Eigen::Matrix4f init_guess = (init_translation * init_rotation).matrix ();

    icp.align(*transformed_cloud_ptr, init_guess);

    Eigen::Matrix4f t = icp.getFinalTransformation();
    Eigen::Matrix4f curr_tf =  t * _prev_tf;

    Eigen::Matrix3f rotation; //rotation matrix
    Eigen::Vector3f translation; //translation vector

    translation << curr_tf(0,3), curr_tf(1,3), curr_tf(2,3);
    rotation << curr_tf(0,0), curr_tf(0,1), curr_tf(0,2),
                curr_tf(1,0), curr_tf(1,1), curr_tf(1,2),
                curr_tf(2,0), curr_tf(2,1), curr_tf(2,2);

    Eigen::Quaternionf quat(rotation); //rotation matrix stored as a quaternion

    geometry_msgs::PoseWithCovarianceStamped curr_pose;
    curr_pose.header.stamp = ros::Time::now();
    curr_pose.header.frame_id = "icp";
    curr_pose.pose.pose.position.x = translation[0];
    curr_pose.pose.pose.position.y = translation[1];        
    curr_pose.pose.pose.position.z = translation[2];        
    curr_pose.pose.pose.orientation.x = quat.x();
    curr_pose.pose.pose.orientation.y = quat.y();        
    curr_pose.pose.pose.orientation.z = quat.z();        
    curr_pose.pose.pose.orientation.w = quat.w();

    pose_pub.publish(curr_pose); //publishing the current pose

    // update transformation matriz
    _prev_tf = curr_tf;

    return;
}