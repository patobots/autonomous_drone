




#include <ros/ros.h>
#include "ICP.h"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "icp_node");
    ros::NodeHandle node;
    ros::NodeHandle private_nh("~");

    ICP icp(node, private_nh); 

    ros::Rate loop_rate(0.5);
    ROS_INFO("Starting ... \n");

    while(ros::ok())
    {
      ros::spinOnce(); //invokes callback
      // loop_rate.sleep();
    }
    node.shutdown();    

    return 0;
}
