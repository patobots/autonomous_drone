# Autonomous Drone

Project [Wiki](https://gitlab.com/patobots/autonomous-drone/wikis/home)

A simple demonstration:

[![image](images/demo.png)](https://drive.google.com/file/d/1hRtQN8vndNiQ3EsC_wZ4L-FGpKo-4UHX/view?usp=sharing)

## Installation

Requirements:

* ROS Noetic - Get Started: [here](http://wiki.ros.org/noetic/Installation).
* Choose the `Desktop-Full` Install

Do not forget to add this step:

    $ echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
    $ source ~/.bashrc

## Clone Repository

    $ cd ~
    $ git clone git@gitlab.com:patobots/autonomous_drone.git
    $ cd ~/autonomous_drone
    $ git submodule update --init --recursive


### Build PX4-Autopilot

#### First Build (Using the jMAVSim Simulator)

First we'll build a simulated target using a console environment. This allows us to validate the system setup before moving on to real hardware and an IDE.

Navigate into the PX4-Autopilot directory and start jMAVSim using the following command:

    $ cd ~/autonomous_drone/PX4-Autopilot
    $ bash ./Tools/setup/ubuntu.sh
    $ make px4_sitl jmavsim

The drone can be flown by typing:

    $ pxh> commander takeoff

#### Build (Using the Gazebo Simulator)

    $ cd ~/autonomous_drone/PX4-Autopilot
    $ make px4_sitl gazebo

## Usage

### MAVSDK C++ (without ROS)
First, download the prebuilt C++ library:

    $ wget https://github.com/mavlink/MAVSDK/releases/download/v0.37.0/mavsdk_0.37.0_ubuntu20.04_amd64.deb
    $ sudo dpkg -i mavsdk_0.37.0_ubuntu20.04_amd64.deb
    $ sudo rm mavsdk_0.37.0_ubuntu20.04_amd64.deb

The Mavsdk examples can be dowloaded by MAVSDK github:

    $ cd /tmp/
    $ git clone https://github.com/mavlink/MAVSDK.git
    $ mv MAVSDK/examples/ ~/autonomous_drone/

Build the example:

    $ cd  ~/autonomous_drone/examples/takeoff_land/
    $ mkdir build && cd build
    $ cmake .. && make

Now run a Gazebo Simulation:

    $ cd ~/autonomous_drone/PX4-Autopilot
    $ make px4_sitl gazebo

and run example:

    $ cd  ~/autonomous_drone/examples/takeoff_land/build/
    $ ./takeoff_and_land udp://:14540


### ROS Environment (WIP)

#### Build ROS Packages

    $ sudo apt-get install ros-noetic-mavros ros-noetic-mavros-extras
    $ wget https://raw.githubusercontent.com/mavlink/mavros/master/mavros/scripts/install_geographiclib_datasets.sh
    $ sudo bash ./install_geographiclib_datasets.sh   
    $ rm install_geographiclib_datasets.sh

Now you are init the `drone_ws`:

    $ cd drone_ws 
    $ catkin init
    $ catkin build

...

# References:
1. [Ubuntu Development Environment
](https://docs.px4.io/master/en/dev_setup/dev_env_linux_ubuntu.html)
1. [ROS with MAVROS Installation Guide](https://docs.px4.io/master/en/ros/mavros_installation.html)
